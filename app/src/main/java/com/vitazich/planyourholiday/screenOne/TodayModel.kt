package com.vitazich.planyourholiday.screenOne

import com.google.gson.annotations.SerializedName

// @Serializable
data class TodayModel(


    //all fields from the API

//    @SerializedName("counties")
//    var counties: List<String>?,

    @SerializedName("countryCode")
    var countryCode: String,

    @SerializedName("date")
    var date: String,
//
//    @SerializedName("fixed")
//    var fixed: Boolean,
//
//    @SerializedName("global")
//    var global: Boolean,
//
//    @SerializedName("launchYear")
//    var launchYear: Int?,

    @SerializedName("localName")
    var localName: String,
//
    @SerializedName("name")
    var name: String,
//
//    @SerializedName("type")
//    var type: String

)


