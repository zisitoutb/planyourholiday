package com.vitazich.planyourholiday.screenOne

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vitazich.planyourholiday.R
import kotlinx.android.synthetic.main.activity_array_json.*
import android.widget.ArrayAdapter
import com.vitazich.planyourholiday.RVAdapter
import java.util.*


class TodayActivity : AppCompatActivity() {


// Datasotre deprecated
//    lateinit var userPreferences: UserPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_array_json)
        setContentView(R.layout.activity_array_json)
        var itemsArray2: ArrayList<TodayCell> = ArrayList()
        // Clean TextViews
        json_results_textview.text = ""

        // cant use until Retrofit consumer error is solved

//        getHolidayToday("BO", itemsArray2, txtPHToday)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.country_list, android.R.layout.simple_spinner_item
        )
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        spinCountry.adapter = adapter


        spinCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                var countrySpin = spinCountry.selectedItem.toString()
                var itemsArray: ArrayList<TodayCell> = ArrayList()
// getting json data via Retrofit based on the spinner
                getHolidayList(countrySpin, itemsArray, json_results_textview, json_results_recyclerview)
                setupRecyclerView()
            }

        }

   }

// setup Recycle View
    fun setupRecyclerView() {
        var layoutManager = LinearLayoutManager(this)
        json_results_recyclerview.layoutManager = layoutManager
        json_results_recyclerview.setHasFixedSize(true)
        var dividerItemDecoration =
            DividerItemDecoration(json_results_recyclerview.context, layoutManager.orientation)
        ContextCompat.getDrawable(this, R.drawable.line_divider)?.let { drawable ->
            dividerItemDecoration.setDrawable(drawable)
        }
        json_results_recyclerview.addItemDecoration(dividerItemDecoration)
    }


}


