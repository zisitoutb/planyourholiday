package com.vitazich.planyourholiday.screenOne

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.vitazich.planyourholiday.R
import com.vitazich.planyourholiday.RVAdapter
import com.vitazich.planyourholiday.network.connAPI
import kotlinx.android.synthetic.main.activity_array_json.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.ArrayList


fun getHolidayList(
    countrySpin: String,
    itemsArray: ArrayList<TodayCell>,
    textjsonView: TextView,
    recyclerjsonView: RecyclerView

) {
    lateinit var adapter: RVAdapter
//        // Create Service
    val service = connAPI.retrofitService
    CoroutineScope(Dispatchers.IO).launch {

        // Do the GET request and get response

        val response = service.getNext(countrySpin)

        withContext(Dispatchers.Main) {
            if (response.isSuccessful) {

                // Convert raw JSON to pretty JSON using GSON library
                val gson = GsonBuilder().setPrettyPrinting().create()
                val prettyJson = gson.toJson(response.body())
                Log.d("Pretty Printed JSON :", prettyJson)
//                    json_results_textview.text = prettyJson
                textjsonView.text = prettyJson

                val items = response.body()
                if (items != null) {
                    for (i in 0 until items.count()) {

                        val datum = items[i].date ?: "N/A"
                        Log.d("Kod zeme: ", datum)

                        val localName = items[i].localName ?: "N/A"
                        Log.d("Nazev svatku lokalne: ", localName)

                        val engName = items[i].name ?: "N/A"
                        Log.d("Nazev svatku anglicky: ", engName)

//                            val datum = items[i].date ?: "N/A"
//                            Log.d("Datum: ", datum)

                        val model =
                            TodayCell(datum, localName, engName)
                        itemsArray.add(model)

                        adapter = RVAdapter(itemsArray)
                        adapter.notifyDataSetChanged()
                    }
                }

                // Pass the Array with data to RecyclerView Adapter
//                    json_results_recyclerview.adapter = adapter
                recyclerjsonView.adapter = adapter

            } else {

                Log.e("RETROFIT_ERROR", response.code().toString())

            }
        }
    }

}

//  Function deprecated due to missing body
//fun getHolidayToday(
//    countrySpin: String,
//    itemsArray: ArrayList<TodayCell>,
//    textjsonView: TextView
//
//) {
////    lateinit var adapter: RVAdapter
////        // Create Service
//    val service = connAPI.retrofitService
//    CoroutineScope(Dispatchers.IO).launch {
//
//        // Do the GET request and get response
//
//        val response = service.getToday(countrySpin)
//
//        withContext(Dispatchers.Main) {
//            when { (response.code() == 200) -> {
//
//                // Convert raw JSON to pretty JSON using GSON library
////                val gson = GsonBuilder().setPrettyPrinting().create()
////                val prettyJson = gson.toJson(response.body())
////                Log.d("Pretty Printed JSON :", prettyJson)
//////                    json_results_textview.text = prettyJson
//////                textjsonView.text = prettyJson
////
////                val items = response.body()
////                if (items != null) {
////                    for (i in 0 until items.count()) {
////
////                        val datum = items[i].date ?: "N/A"
////                        Log.d("Kod zeme: ", datum)
////
////                        val localName = items[i].localName ?: "N/A"
////                        Log.d("Nazev svatku lokalne: ", localName)
////
////                        val engName = items[i].name ?: "N/A"
////                        Log.d("Nazev svatku anglicky: ", engName)
//
////                            val datum = items[i].date ?: "N/A"
////                            Log.d("Datum: ", datum)
//                        textjsonView.text = "Yes!! today is the public holiday."
////                        val model =
////                            TodayCell(datum, localName, engName)
////                        itemsArray.add(model)
////
////                        adapter = RVAdapter(itemsArray)
////                        adapter.notifyDataSetChanged()
//
//
//
//                // Pass the Array with data to RecyclerView Adapter
////                    json_results_recyclerview.adapter = adapter
////                recyclerjsonView.adapter = adapter
//
//            }
//                (response.code() == 204) -> {
//                    Log.i("RETROFIT", "TODAY IS NOT THE DAY")
//                    textjsonView.text = "TODAY IS NOT THE DAY"
//                }
//                else -> {
//                    Log.e("RETROFIT_ERROR", response.code().toString())
//                            textjsonView.text = "RETROFIT_ERROR" }
//            }
//        }
//    }
//
//}


