package com.vitazich.planyourholiday.db

import android.content.Context
import androidx.lifecycle.LiveData

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class UserRepository {

    companion object {

        var userDatabase: UserDatabase? = null

        var userTableModel: LiveData<UserTableModel>? = null

        var userTableModelPref: LiveData<UserTableModel>? = null

        var userTableModelCountry: LiveData<UserTableModel>? = null

        fun initializeDB(context: Context) : UserDatabase {
            return UserDatabase.getDataseClient(context)
        }

        fun insertData(context: Context, username: String, password: String, countryPref: String) {

            userDatabase = initializeDB(context)

            CoroutineScope(IO).launch {
                val loginDetails = UserTableModel(username, password, countryPref)
                userDatabase!!.userDao().InsertData(loginDetails)
            }

        }

        fun getLoginDetails(context: Context, username: String) : LiveData<UserTableModel>? {

            userDatabase = initializeDB(context)

            userTableModel = userDatabase!!.userDao().getLoginDetails(username)

            return userTableModel
        }

        fun checkLogin(context: Context, username: String, password: String) : LiveData<UserTableModel>? {

            userDatabase = initializeDB(context)

            userTableModelPref = userDatabase!!.userDao().checkLogin(username, password)

            return userTableModelPref
        }

        fun getCountry(context: Context, username: String) : LiveData<UserTableModel>? {

            userDatabase = initializeDB(context)

            userTableModelCountry = userDatabase!!.userDao().getCountry(username)

            return userTableModelCountry
        }


    }
}