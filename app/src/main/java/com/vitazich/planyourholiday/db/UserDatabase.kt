package com.vitazich.planyourholiday.db

import android.content.Context
import androidx.room.*

@Database(entities = arrayOf(UserTableModel::class), version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {

    abstract fun userDao() : DAOAccess

    companion object {

        @Volatile
        private var INSTANCE: UserDatabase? = null

        fun getDataseClient(context: Context) : UserDatabase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, UserDatabase::class.java, "LOGIN_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }

    }

}