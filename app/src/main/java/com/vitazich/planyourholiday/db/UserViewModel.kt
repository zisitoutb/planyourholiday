package com.vitazich.planyourholiday.db

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class UserViewModel : ViewModel() {

    var liveDataUser: LiveData<UserTableModel>? = null
    var liveDataCredentials: LiveData<UserTableModel>? = null
    var liveDataCountry: LiveData<UserTableModel>? = null

    fun insertData(context: Context, username: String, password: String, countryPref: String) {
       UserRepository.insertData(context, username, password, countryPref)
    }

    fun getLoginDetails(context: Context, username: String) : LiveData<UserTableModel>? {
        liveDataUser = UserRepository.getLoginDetails(context, username)
        return liveDataUser
    }

    fun checkLogin(context: Context, username: String, password: String) : LiveData<UserTableModel>? {
        liveDataCredentials = UserRepository.checkLogin(context, username, password)
        return liveDataCredentials
    }

    fun getCountry(context: Context, username: String) : LiveData<UserTableModel>? {
        liveDataCountry = UserRepository.getCountry(context, username)
        return liveDataCountry
    }


}