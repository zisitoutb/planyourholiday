package com.vitazich.planyourholiday.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DAOAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertData(userTableModel: UserTableModel)

    @Query("SELECT * FROM LoginActivity WHERE Username =:username")
    fun getLoginDetails(username: String?) : LiveData<UserTableModel>

    @Query("SELECT * FROM LoginActivity WHERE Username =:username AND Password =:password")
    fun checkLogin(username: String?, password: String?) : LiveData<UserTableModel>
//    fun checkLogin(username: String?, password: String?) : List<UserTableModel>

    @Query("SELECT * FROM LoginActivity WHERE Username =:username")
    fun getCountry(username: String?) : LiveData<UserTableModel>

}