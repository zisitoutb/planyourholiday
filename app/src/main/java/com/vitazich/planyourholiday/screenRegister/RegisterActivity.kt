package com.vitazich.planyourholiday.screenRegister

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.vitazich.planyourholiday.R
import com.vitazich.planyourholiday.db.UserViewModel
import com.vitazich.planyourholiday.screenOne.TodayActivity
import kotlinx.android.synthetic.main.activity_array_json.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.btnDoLogin
import kotlinx.android.synthetic.main.activity_register.txtPassword
import kotlinx.android.synthetic.main.activity_register.txtUsername


class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        lateinit var userViewModel: UserViewModel

        lateinit var context: Context

        lateinit var strUsername: String
        lateinit var strPassword: String
        lateinit var countryPref: String

            context = this

            userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

// Saving new user to database
            btnDoLogin.setOnClickListener {

                strUsername = txtUsername.text.toString().trim()
                strPassword = txtPassword.text.toString().trim()
                countryPref = spinCountryPref.selectedItem.toString().trim()

                if (strPassword.isEmpty()) {
                    txtUsername.error = "Please enter the username"
                }
                else if (strPassword.isEmpty()) {
                    txtPassword.error = "Please enter the username"
                }
                else {
                    userViewModel.insertData(context, strUsername, strPassword, countryPref)
                    Toast.makeText(
                        applicationContext,
                        "Account created",
                        Toast.LENGTH_SHORT
                    ).show()
//                    btnToRegister.text = "Inserted Successfully"
//                    txtCountry.text = countryPref

                    val intent = Intent(this, TodayActivity::class.java)
                    this.startActivity(intent)
                }
            }





        }


    }


