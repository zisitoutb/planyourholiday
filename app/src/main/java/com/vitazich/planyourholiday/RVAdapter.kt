package com.vitazich.planyourholiday

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vitazich.planyourholiday.screenOne.TodayCell
import kotlinx.android.synthetic.main.cell.view.*

class RVAdapter(private val cell: ArrayList<TodayCell>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val vh = LayoutInflater.from(parent.context).inflate(R.layout.cell, parent, false)
        return ViewHolder(vh)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.country_id_textview.text = cell[position].dttm
        holder.itemView.localname_name_textview.text = cell[position].localName
        holder.itemView.engname_textview.text = cell[position].engName
//        holder.itemView.country_id_textview.text = cell[position].country
    }

    override fun getItemCount(): Int {
        return cell.size
    }
}