package com.vitazich.planyourholiday.network

import com.vitazich.planyourholiday.screenOne.TodayModel
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

private const val BASE_URL = "https://date.nager.at"
//https://date.nager.at
    //"http://172.24.23.7:80/"

val retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()


interface connectorService {
//    @GET("Api/v1/Get/CZ/2021") //
//    suspend fun getCZ(): Response<List<TodayModel>>
    @GET("Api/v2/NextPublicHolidays/{country}") //
    suspend fun getNext(@Path("country", encoded = false) country: String): Response<List<TodayModel>>
    //Call<List<Holidays?>?>?

    @GET("Api/v2/IsTodayPublicHoliday/{country}") //
    suspend fun getToday(@Path("country", encoded = false) country: String): Response<List<TodayModel>>

}

object connAPI {
    val retrofitService : connectorService by lazy {
        retrofit.create(connectorService::class.java)
    }
}