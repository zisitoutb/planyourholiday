package com.vitazich.planyourholiday.screenLogin

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.vitazich.planyourholiday.R
import com.vitazich.planyourholiday.db.UserViewModel
import com.vitazich.planyourholiday.screenOne.TodayActivity
import com.vitazich.planyourholiday.screenRegister.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.btnDoLogin
import kotlinx.android.synthetic.main.activity_register.txtPassword
import kotlinx.android.synthetic.main.activity_register.txtUsername

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        lateinit var userViewModel: UserViewModel

        lateinit var context: Context

        lateinit var strUsername: String
        lateinit var strPassword: String
        lateinit var strCountry: String
        context = this



        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        strUsername = txtUsername.text.toString().trim()
        strPassword = txtPassword.text.toString().trim()




// Login screen check

        if (strPassword.isEmpty()) {
            txtUsername.error = "Please enter the username"
        } else if (strPassword.isEmpty()) {
            txtPassword.error = "Please enter the username"
        } else {
            userViewModel.checkLogin(context, strUsername, strPassword)

        }
// Login credentials check
            btnDoLogin.setOnClickListener {

                strUsername = txtUsername.text.toString().trim()
                strPassword = txtPassword.text.toString().trim()
//                strCountry = txtCountry.text.toString().trim()

                userViewModel.checkLogin(context, strUsername, strPassword)!!
                    .observe(this, Observer {

                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                "Invalid credentials",
                                Toast.LENGTH_SHORT
                            ).show()

//                    lblUseraname.text = "- - -"
//                    lblPassword.text = "- - -"
                        } else {
//                            txtUsername.text = it.Username
//                            txtPassword.text = it.Password
//                             = it.PrefCountry

                            val a = it.Password
                            val b = it.Username
                            val countryUser = it.PrefCountry
                            val d = println(countryUser.javaClass.kotlin.simpleName)
//                            lblPassword.text = it.Password
                            Toast.makeText(
                                applicationContext,
                                "User ${b} successfully logged in",
                                Toast.LENGTH_SHORT
                            ).show()


                            val intent = Intent(this, TodayActivity::class.java)
                            this.startActivity(intent)
                        }
                    })
            }

            btnToRegister.setOnClickListener {

//            strUsername = txtUsername.text.toString().trim()

// Forward to the next screen
                val intent = Intent(this, RegisterActivity::class.java)
                this.startActivity(intent)
            }



    }}
